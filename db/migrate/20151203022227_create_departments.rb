class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.string :name
      t.string :code
      
      t.references :college, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
