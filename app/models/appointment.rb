class Appointment < ActiveRecord::Base
  belongs_to :advisor, :class_name => 'User', :foreign_key => 'advisor_id'
  belongs_to :student, :class_name => 'User', :foreign_key => 'student_id'
  
  belongs_to :note

  after_save :check_note
  before_save :update_end_time
  
  validates :advisor_id, presence: true
  validates :student_id, presence: true

  
  
  private
  def check_note
    # st = self.start_time
    # self.update_attributes(:end_time => st + 30*60)
    
    if self.note.nil?
      _note = Note.create
      self.update_attributes(:note => _note)
    end

    # only send an email if the start time has changed
    if start_time_changed?
      UserMailer.appointment_notification_email(self.student, self).deliver_now
      UserMailer.appointment_notification_email_to_advisor(self.advisor, self).deliver_now
    end
  end

  def update_end_time
    self.end_time = self.start_time + 60*30
  end
  
end
